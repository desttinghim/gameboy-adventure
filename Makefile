##
# GBC Adventure
#
# @file
# @version 0.1

SRC := src
BIN := bin
ASSETS := assets
ASSETS_TARGET := $(BIN)/tileset.2bpp
PALETTE_TARGET := $(BIN)/tileset.pal
TARGET := $(BIN)/gbcadventure.gbc

all: $(TARGET)

clean:
	rm -r $(BIN)

$(BIN):
	mkdir -p $(BIN)

$(TARGET): $(SRC)/main.asm $(ASSETS_TARGET) $(BIN)
	rgbasm -o $(BIN)/main.o $(SRC)/main.asm
	rgblink -o $(TARGET) -n $(BIN)/main.sym $(BIN)/main.o
	rgbfix -v -p 0 $(TARGET)

$(ASSETS_TARGET): $(ASSETS)/*.png  $(BIN)
	rgbgfx -u -t $(BIN)/tilemap.tilemap -o $(BIN)/tilemap.2bpp $(ASSETS)/tilemap.png
	rgbgfx -h -o $(BIN)/detective.2bpp $(ASSETS)/detective.png
	rgbgfx -h -o $(BIN)/dwarf.2bpp $(ASSETS)/dwarf.png

run: $(TARGET)
	bgb $(TARGET)


# end
