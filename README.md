# GBC Adventure

Trying to write an adventure game for the Gameboy Color.

To try it out, install `rgbds` and `bgb`, then run the following commands:

``` sh
make all
make run
```
