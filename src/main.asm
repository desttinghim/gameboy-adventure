INCLUDE "inc/hardware.inc"

dcolor: MACRO  ; $rrggbb -> gbc representation
_r = ((\1) & $ff0000) >> 16 >> 3
_g = ((\1) & $00ff00) >> 8  >> 3
_b = ((\1) & $0000ff) >> 0  >> 3
    dw (_r << 0) | (_g << 5) | (_b << 10)
ENDM

DisableLCD: MACRO
    ld a, [rLCDC]
    and a, $ff & ~LCDCF_ON
    ld [rLCDC], a
ENDM

EnableLCD: MACRO
    ld a, [rLCDC]
    or a, LCDCF_ON
    ld [rLCDC], a
ENDM

SECTION "Vblank interrupt", ROM0[$0040]
    push hl
    ld hl, vblank_flag
    ld [hl], 1
    pop hl
    reti

SECTION "LCD controller status interrupt", ROM0[$0048]
    ; Fires on a handful of selectable LCD conditions, e.g.
    ; after repainting a specific row on the screen
    reti

SECTION "Timer overflow interrupt", ROM0[$0050]
    ; Fires at a configurable fixed interval
    reti

SECTION "Serial transfer completion interrupt", ROM0[$0058]
    ; Fires when the serial cable is done?
    reti

SECTION "P10-P13 signal low edge interrupt", ROM0[$0060]
    ; Fires when a button is released?
    reti

SECTION "Utilities", ROM0
;;; Copy c bytes from de to hl
;;; NOTE: c = 0 means to copy 256 bytes
copy:
    ld a, [de]
    inc de
    ld [hl+], a
    dec c
    jr nz, copy
    ret

wait_for_vblank:
    xor a
    di                          ; clear vblank flag
    ld [vblank_flag], a         ; avoid irq race after this ld

.vblank_loop:
    ei
    halt                        ; wait for interrupt
    di
    ld a, [vblank_flag]         ; Was it a vblank interrupt?
    and a
    jr z, .vblank_loop           ; If not, keep waiting
    ei
    ret


SECTION "init", ROM0[$0100]
    nop
    jp start


SECTION "DMA", ROM0
dma_copy:
    ld de, wShadowOAM
    ld a, d
    ld [rDMA], a
    ld a, 40
.loop:
    dec a
    jr nz, .loop
    ret
dma_copy_end:
    nop

SECTION "main", ROM0[$0150]
start:
    ;; Enable interrupts
    ld a, IEF_VBLANK
    ldh [rIE], a
    ei

    call wait_for_vblank

    ;; Set LCD palette for greyscale mode
    ld a, %11100100
    ldh [rBGP], a
    ldh [rOBP0], a

    ;; LCD controller
    ld a, LCDCF_OBJ16 | LCDCF_OBJON | LCDCF_BGON
    ld [rLCDC], a

    DisableLCD

    ;; Load tileset
    ld hl, $9000
    ld bc, Tileset
    ld d,  LOW((TilesetEnd - Tileset) / 8)
.loadTiles
    ld e, 16
.loadTile
    ld a, [bc]
    inc bc
    ld [hli], a
    dec e
    jr nz, .loadTile
    dec d
    jr nz, .loadTiles

    ;; Load tilemap
    ld hl, vScreen
    ld bc, Tilemap
    ld d, SCRN_VY_B
.loadTilemap
    ld e, SCRN_VX_B
.loadTilemapTile
    ld a, [bc]
    inc bc
    ld [hli], a
    dec e
    jr nz, .loadTilemapTile
    dec d
    jr nz, .loadTilemap

    ;; Define an object
    ld hl, $8800
    ld de, SpriteDetective
    ld c, LOW(SpriteDetectiveEnd - SpriteDetective)
.loadDetective
    ld a, [de]
    ld [hl+], a
    inc de
    dec c
    jr nz, .loadDetective

    ld de, SpriteDwarf
    ld c, LOW(SpriteDwarfEnd - SpriteDwarf)
.loadDwarf
    ld a, [de]
    ld [hl+], a
    inc de
    dec c
    jr nz, .loadDwarf

    ;; Put an object on screen
    ld hl, wShadowOAM
    ld de, .oam
    ld c, .oamEnd - .oam
    call copy
    ld b, 40 - ((.oamEnd - .oam) / 4)
    xor a
.clearExtraSprites                     ; Init OAM buffer to 0
    ld [hli], a
    ld [hli], a
    ld [hli], a
    ld [hli], a
    dec b
    jr nz, .clearExtraSprites

    ;; Copy the little DMA routine into high RAM
    ld de, dma_copy
    ld hl, $ff80
    ld c, dma_copy_end - dma_copy
    call copy

    EnableLCD

    jp main

.oam
    ;; detective
    db 48, 16, 128, 0
    db 48, 16 + 8, 130, 0
    ;; dwarf
    db 48, 32, 132, 0
    db 48, 32 + 8, 134, 0
.oamEnd


main:
    call wait_for_vblank

    xor a, a                    ; Always sets a to 0, shorter/faster than ld a, 0
    ld [vblank_flag], a         ; Set vblank_flag to 0

    call $ff80                  ; Use DMA to update object attribute memory

    ;; Update everything else here

    ;; Poll input
    ;; Getting a reliable read takes a few moments, so the official manual recommends
    ;; reading multiple times

    ;; Bit 5 reads dpad
    ;; (Actually bit 4 being off means read dpad)
    ld a, $20
    ldh [rP1], a
    ;; Unreliable, do it twice
    ld a, [rP1]
    ld a, [rP1]
    ;; 'complement' instruction, flips all bits in a
    ;; needed because a bit set to 1 means the button is released
    cpl
    ;; Store lower four bits in b
    and a, $0f
    ld b, a

    ;; Bit 4 means to read the buttons
    ;; (Same caveat; it's really that bit 5 is off)
    ld a, $10
    ldh [rP1], a
    ;; Apparently more stalling is needed here
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    ld a, [rP1]
    cpl
    and a, $0f
    swap b                      ; Swaps low and high nybble in b
    or a, b                     ; Put DPAD in high nybble
    ld [buttons], a             ; Store input byte in RAM

    ;; Move sprite in wShadowOAM
    ld hl, wShadowOAM           ; Point to sprite from RAM
    ld b, [hl]                  ; Store y in b
    inc hl
    ld c, [hl]                  ; Store x in c

    ;; Sets the z flag to match a particular bit in register a
    bit PADB_LEFT, a
    jr z, .skip_left            ; If z the bit is zero, so skip decrementing x
    dec c
.skip_left:
    bit PADB_RIGHT, a
    jr z, .skip_right
    inc c
.skip_right:
    bit PADB_UP, a
    jr z, .skip_up
    dec b
.skip_up:
    bit PADB_DOWN, a
    jr z, .skip_down
    inc b
.skip_down:

.movement
    ld [hl], c                  ; Write new coordinates back to wShadowOAM
    ld a, [hl]
    dec hl
    ld [hl], b
    ld a, [hl]
    ld a, c                     ; Move bottom sprite too
    add a, 8
    ld hl, wShadowOAM + 5
    ld [hl], a
    dec hl
    ld [hl], b

.collision
    ld hl, wShadowOAM
    ld a, [hli]   ; get y
    rrca          ; shift right 4 (equivalent to division by 32)
    rrca
    rrca
    and %00011111
    sla a          ; shift left by 4 to floor
    sla a
    sla a
    ;; net result is rounding to nearest multiple of 8
    ld d, a
    ld a, [hli]   ; get x
    rrca
    rrca
    rrca
    and %00011111
    add d         ; current pos now stored in d
    ld d, a
    ld bc, vScreen
    ld a, c
    add a, d
    ld c, a
    ld h, b
    ld l, c
    ;; Player's position is now stored in hl
    inc hl
    ld a, [hl]
    cp 2

    jr nc, .end
    jr z, .end
    ld d,d
    jr .end
    dw $6464
    dw $0000
    db "collision"
.end

    jp main                     ; Start it all over again


SECTION "Graphics", ROM0
Tileset:
    INCBIN "bin/tilemap.2bpp"
TilesetEnd:
Tilemap:
    INCBIN "bin/tilemap.tilemap"
TilemapEnd:
SpriteDetective:
    INCBIN "bin/detective.2bpp"
SpriteDetectiveEnd:
SpriteDwarf:
    INCBIN "bin/dwarf.2bpp"
SpriteDwarfEnd:

SECTION "Important twiddles", WRAM0[$C000]
;; Reserve a byte in working RAM to use as the vblank flag
vblank_flag:
    db
buttons:
    db

SECTION "OAM Buffer", WRAM0[$C100]
wShadowOAM:
    ds 4 * 40
wShadowOAMEnd: 

SECTION UNION "9800 tilemap", VRAM[_SCRN0],BANK[0]
vScreen:
    ds SCRN_Y_B * SCRN_VX_B      ; As many tilemap rows as there are screen rows
vScreenEnd:
